# Features

- Ray Marching algorithm in GLSL
- Signed distance functions for cube, sphere, and distorted sphere
- Mirror or glass surfaces
- Debug view with number of steps to hit a surface
- Distance plane with distance to the closest object
- Skybox with sun
- Shadows
- Qt widgets to edit scene in real time
- Hot reload of glsl shader at runtime

## Results

### Spheres
![](images/spheres.png)

### Spheres 2
![](images/spheres2.png)

### Mirror cubes
![](images/mirror.png)

### Transparency with refraction
![](images/transparent.png)

### Distorted spheres
![](images/distorted_spheres.png)

### Qt window
![](images/widgets.png)


## Build

```
mkdir build
cd build
cmake ..
make
..
build/IN55
```

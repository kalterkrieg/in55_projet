#version 330

#define PI 3.14159265

#define DPLANE 1
#define OBJECT1 2
#define OBJECT2 3
#define OTHER 4

#define DPLANEMATERIAL 1
#define GLASSMATERIAL 2
#define MIRRORMATERIAL 3
#define PLAINMATERIAL 4

#define CUBESHAPE 1
#define SPHERESHAPE 2
#define DISTOSPHERESHAPE 3

struct Camera{
    vec3 position;
    mat4 viewMatrix;
};

struct Scene{
    float objectId;
    float materialId;
    float minGlobal;    // minimum distance to anything, including distance plane
    float minToObject;  // minimum distance to anything but the distance plane
};


in vec4 gl_FragCoord;
out vec4 fragColor;

uniform Camera cam;
uniform vec2 screenSize;
uniform int antialiasing;
uniform float object1Shape;
uniform float object2Shape;
uniform float object1Material;
uniform float object2Material;

bool object1Hit = false;
bool object2Hit = false;

// Maximum/minumum elements of a vector
float vmax(vec2 v) {
	return max(v.x, v.y);
}

float vmax(vec3 v) {
	return max(max(v.x, v.y), v.z);
}

float vmax(vec4 v) {
	return max(max(v.x, v.y), max(v.z, v.w));
}

float vmin(vec2 v) {
	return min(v.x, v.y);
}

float vmin(vec3 v) {
	return min(min(v.x, v.y), v.z);
}

float vmin(vec4 v) {
	return min(min(v.x, v.y), min(v.z, v.w));
}

// distance function of a sphere
float fSphere(vec3 p, float r) {
	return length(p) - r;
}

// Plane with normal n (n is normalized) at some distance from the origin
float fPlane(vec3 p, vec3 n, float distanceFromOrigin) {
	return dot(p, n) + distanceFromOrigin;
}

// exact distance function of a box
float fBox(vec3 p, vec3 b) {
	vec3 d = abs(p) - b;
	return length(max(d, vec3(0))) + vmax(min(d, vec3(0)));
}

// distance function of a distorted sphere
float fDistortedSphere(vec3 p, float r) {
    float displacement = sin(distortion * p.x) * sin(distortion * p.y) * sin(distortion * p.z) * 0.25;
	return length(p) - r + displacement;
}

Scene map_the_world(in vec3 p)
{
    float distancePlane = fPlane(p, vec3(0, 1, 0), -planeHeight);
    // create a plane with normal (0, 1, 0) with a distance depending of the corresponding widget

    p.y -= 5.5; // move the world down (so move the objects up)
    p.x += 5;   // idem but move left

    float object1;
    float object2;

    // create first object
    if(object1Shape == SPHERESHAPE)
        object1 = fSphere(p, objectsSize);
    else if(object1Shape == CUBESHAPE)
        object1 = fBox(p, vec3(objectsSize));
    else
        object1 = fDistortedSphere(p, objectsSize);

    p.x -= 10;

    // create second object
    if(object2Shape == SPHERESHAPE)
        object2 = fSphere(p, objectsSize);
    else if(object2Shape == CUBESHAPE)
        object2 = fBox(p, vec3(objectsSize));
    else
        object2 = fDistortedSphere(p, objectsSize);

    p.x += 5;
    p.y += 5.5;

    // object1 and object2 now contains the value of the distance from the current
    // point in space to the closest point of their surface

    float wallsDistance = 30;

    // create walls
    p.x -= wallsDistance;
    float walls = fBox(p, vec3(0.4, 10, wallsDistance));
    p.x += wallsDistance*2;

    walls = min(walls,fBox(p, vec3(0.4, 10, wallsDistance)));
    p.x -= wallsDistance;
    p.z += wallsDistance;

    walls = min(walls,fBox(p, vec3(wallsDistance, 10, 0.4)));
    p.z -= wallsDistance*2;

    walls = min(walls,fBox(p, vec3(wallsDistance, 10, 0.4)));
    p.z += wallsDistance;


    float scene = walls;

    if(!object1Hit)
        scene = min(scene, object1);

    if(!object2Hit)
        scene = min(scene, object2);
    // if objects have already been hit, ignore them

    float objectId;
    float materialId;
    float minGlobal;

    if(activateDistancePlane)
        minGlobal = min(distancePlane, scene);
    else
        minGlobal = scene;

    // set the objectId and materialId depending on the closest object
    if (minGlobal==distancePlane && activateDistancePlane){
        objectId = DPLANE;
        materialId = DPLANEMATERIAL;
    }
    else if (minGlobal == object1){
        objectId = OBJECT1;
        materialId = object1Material;
    }
    else if (minGlobal == object2)
    {
        objectId = OBJECT2;
        materialId = object2Material;
    }
    else {
        objectId = OTHER;
        materialId = PLAINMATERIAL;
    }

    return Scene(objectId, materialId, minGlobal, scene);
}

vec3 calculate_normal(in vec3 p)
{
// calculate a normal using gradient
    const vec3 small_step = vec3(0.001, 0.0, 0.0);

    float gradient_x = map_the_world(p + small_step.xyy).minGlobal - map_the_world(p - small_step.xyy).minGlobal;
    float gradient_y = map_the_world(p + small_step.yxy).minGlobal - map_the_world(p - small_step.yxy).minGlobal;
    float gradient_z = map_the_world(p + small_step.yyx).minGlobal - map_the_world(p - small_step.yyx).minGlobal;

    vec3 normal = vec3(gradient_x, gradient_y, gradient_z);

    return normalize(normal);
}

vec3 planeColor(float distanceToObject){
/*
    Linear RGB interpolation of multiple colors depending on the distance to the nearest
    object
*/

    vec3 firstColor  = vec3(83, 46, 95) / 255;
    vec3 secondColor = vec3(138, 54, 27) / 255;
    vec3 thirdColor = vec3(159, 150, 115) / 255;
    vec3 fourthColor = vec3(170, 170, 168) / 255;

    vec3 color;

    float firstColorDistance = 10;
    float secondColorDistance = 40;
    float thirdColorDistance = 80;

    vec3 a;
    vec3 b;
    float divider, d;

    if(distanceToObject < firstColorDistance){
        a = firstColor;
        b = secondColor;

        d = distanceToObject/firstColorDistance;

    }
    else if (distanceToObject > firstColorDistance && distanceToObject < secondColorDistance){
        a = secondColor;
        b = thirdColor;
        divider = secondColorDistance-firstColorDistance;
        d = (distanceToObject-firstColorDistance)/(secondColorDistance-firstColorDistance);
    }
    else if (distanceToObject > secondColorDistance && distanceToObject < thirdColorDistance){
        a = thirdColor;
        b = fourthColor;
        divider = thirdColorDistance-secondColorDistance;
        d = (distanceToObject-secondColorDistance)/(thirdColorDistance-secondColorDistance);
    }
    else {

        return fourthColor;

    }

    color.x = a.x * (1-d) + b.x * d;
    color.y = a.y * (1-d) + b.y * d;
    color.z = a.z * (1-d) + b.z * d;

    if(mod(distanceToObject, 1) > 0.8)   // every 1 unit, make a .2 wide darker line
        color = vec3(mod(1-distanceToObject, 1));

    return color;
}

vec3 refract(vec3 dir, vec3 nor, float idx)
{
// refract a ray dir depending on normal nor and refraction index idx
    vec3 R;
    float k = 1.0 - idx * idx * (1.0 - dot(nor, dir) * dot(nor, dir));
    if (k < 0.0)
        R = vec3(0.0);
    else
        R = idx * dir - (idx * dot(nor, dir) + sqrt(k)) * nor;

    return R;
}

vec3 reflect(vec3 dir, vec3 nor)
{
// reflect a ray dir depending on normal nor
    return dir - 2.0 * dot(nor, dir) * nor;
}

vec3 ray_march(in vec3 from, in vec3 direction)
{
    const int MAX_STEPS = 300;                      // maximum number iterations of the for loop
    const float MINIMUM_DISTANCE = 0.001;           // distance at which we assume we hit an object
    const float MAXIMUM_TRACE_DISTANCE = 1000.0;    // maximum distance before breaking out of the for loop

    const vec3 sun = vec3(0.577, 0.577,  0.577);    // position of the sun

    float total_distance = 0.0;
    vec3 current_position = from;

    Scene scene = map_the_world(current_position);  // initialize the scene for the first iteration
    float distance_to_closest = scene.minGlobal;

    if(distance_to_closest < 0) // return black if inside an object
        return vec3(0);

    vec3 color = vec3(1);
    int nbsteps = 1;

    for (int i = 0; i < MAX_STEPS; ++i)
    {
        nbsteps = i;
        current_position = current_position + distance_to_closest * direction;

        scene = map_the_world(current_position);
        distance_to_closest = scene.minGlobal;

        if (distance_to_closest < MINIMUM_DISTANCE) // if we hit an object
        {
            vec3 hit_position = current_position;
            vec3 normal = calculate_normal(hit_position);

            if(scene.objectId == DPLANE){ // if the object is the distance plane
                object1Hit = false; // if we see the plane through glass or mirror, we reset object hit so the next call
                object2Hit = false; // to map_the_world takes into account the objects
                color *= planeColor(map_the_world(current_position).minToObject);
                break;
            }
            else if(scene.materialId == GLASSMATERIAL){ // if the object is glass
                if(scene.objectId == OBJECT1){
                    if(!object1Hit){
                        object1Hit = true; // ignore object1 for the next calls to map_the_world
                        direction = refract(direction, normal, 1.0/refractionIndex); // refract the ray
                        color *= vec3(0.66, 0.66, 0.66) + materialTint.rgb/3; // change color depending on widget
                    }
                }
                else if(scene.objectId == OBJECT2){
                    if(!object2Hit){
                        object2Hit = true;
                        direction = refract(direction, normal, 1.0/refractionIndex);
                        color *= vec3(0.66, 0.66, 0.66) + materialTint.rgb/3;
                    }
                }
            }
            else if(scene.materialId == MIRRORMATERIAL){

                if(scene.objectId == OBJECT1){
                    if(!object1Hit){
                        object1Hit = true;
                        object2Hit = false; // if we hit object1, reset object2hit to allow infinite mirrors
                        direction = reflect(direction, normal);
                        color *= vec3(0.66, 0.66, 0.66) + materialTint.rgb/3;
                    }
                }
                else if(scene.objectId == OBJECT2){
                    if(!object2Hit){
                        object2Hit = true;
                        object1Hit = false;
                        direction = reflect(direction, normal);
                        color *= vec3(0.66, 0.66, 0.66) + materialTint.rgb/3;
                    }
                }

            }
            else { // other material
                vec3 mate = vec3(1.0,0.8,0.7)*0.3; // color of a wall

                const vec3 lig = sun;
                float dif = clamp( dot( lig, normal ), 0.1, 1.0 ); // diffuse light
                vec3 hal = normalize( -direction+lig );
                float co = clamp( dot(hal,lig), 0.0, 1.0 );
                float fre = 0.04 + 0.96*pow(1.0-co,5.0); // "damp" specular light
                float spe = pow(clamp(dot(hal,normal), 0.0, 1.0 ), 32.0 ); // specular light

                color *= mate*3.5*vec3(1.00,1,1)*dif; // material color
                color +=  7.0*3.5*vec3(1.00,0.90,0.70)*spe*dif*fre; // light effects

                break;
            }

        }

        if (total_distance > MAXIMUM_TRACE_DISTANCE || i == MAX_STEPS-1) // if really far or max number of steps
        {
            // show the sky
            color *= vec3(0.7,0.9,1.0)*(0.7+0.3*direction.y); // blue gradient
            color += vec3(0.8,0.7,0.5)*pow( clamp(dot(direction,sun),0.0,1.0), 48.0 ); // sun
            break;
        }
        total_distance += distance_to_closest;
    }

    if(showStepNumber)                              // if show number of step checkbox is checked
        return vec3(log(nbsteps))/log(MAX_STEPS);   // show white gradient of the number of steps
    else if(activateShadows){                       // else calculate shadows
        vec3 ray_origin = current_position;
        vec3 ray_direction = sun;
        float res = 1.0;
        float ph = 1e20;
        float mint = 0.1;
        float min_dist = MAXIMUM_TRACE_DISTANCE;
        float maxt = MAXIMUM_TRACE_DISTANCE;
        float closestObjectMaterial = 0;
        int k = 4;
        for( float t=mint; t < maxt; )
        {
            Scene scene = map_the_world(ray_origin + ray_direction*t);
            float h = scene.minToObject;
            if(h<min_dist){
                closestObjectMaterial = scene.materialId;   // save the material of the object closest
                min_dist = h;                               // to the shadow ray in case it graze but doesnt touch
            }
            if( h<0.001 ) // if we touch
            {
                res = 0.0; // hard shadow
                break;
            }
            float y = h*h/(2.0*ph);
            float d = sqrt(h*h-y*y);
            res = min( res, k*d/max(0,t-y) ); // if we dont touch but we are close, shadow is less and less hard
            ph = h;
            t += h;
        }
        if(closestObjectMaterial == GLASSMATERIAL) // if the closest (or hit) material is glass, shadow should be clearer
            res = min(res+0.4, 1);

        res = clamp(res+0.2, 0, 1); // make all shadows a little clearer
        color = res*color; // update the color of the pixel

    }
    return color;
}


#define AA antialiasing

void main()
{
    vec2 uv = gl_FragCoord.xy / screenSize;
    vec2 _uv = uv.xy*2.0-1.0; // transform viewport coordinates to (-1, 1) in both axis
    float ratio = screenSize.x/screenSize.y;
    _uv.y /= ratio;

    vec3 ray_origin = cam.position;
    vec3 color = vec3(0);

    for( int j=0; j<AA; j++ ) // simple antialiasing
    for( int i=0; i<AA; i++ )
    {
        object1Hit = false; // reset object hit for every AA iteration, otherwise we have transparent objects
        object2Hit = false;
        vec3 ray_direction = normalize(vec3(_uv, 1-ratio/4));   // calculate the ray direction, depending on coordinate 
                                                                // in viewport and distance from camera (kind of fov)
        
        ray_direction += vec3(float(i / screenSize.x),float(j / screenSize.y), 0)/float(AA);    // shift the ray a little
                                                                                                // (or not if AA = 1)
        ray_direction = vec3(cam.viewMatrix*vec4(ray_direction,1.0));   // apply view matrix
        color += ray_march(ray_origin, ray_direction);
    }
    fragColor = vec4(color/float(AA*AA), 1.0);
    fragColor = pow( fragColor, vec4(0.8 )); // slight gamma correction

}


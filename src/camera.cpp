//
// Created by kalterkrieg on 16/05/19.
//

#include "camera.h"


Camera::Camera(QVector3D position, QQuaternion orientation) :
        m_position(position),
        m_orientation(orientation) {
    this->m_orientation.normalize();
    buildViewMatrix();
}

void Camera::buildViewMatrix() {
    m_viewMatrix.setToIdentity();
    m_viewMatrix.rotate(m_orientation);
}

const QVector3D &Camera::getPosition() const {
    return m_position;
}

void Camera::setPosition(const QVector3D &position) {
    Camera::m_position = position;
    buildViewMatrix();
}

const QQuaternion &Camera::getOrientation() const {
    return m_orientation;
}

const QMatrix4x4 Camera::getViewMatrix() {
    return m_viewMatrix;
}

void Camera::rotateFromMouseDiff(QVector2D diff) {
    auto rotation = QQuaternion::fromAxisAndAngle({0, 1, 0},
                                                  diff.x() / (1 / m_rotateSpeed)); //create rotation around y axis
    rotate(rotation);
    rotation = QQuaternion::fromAxisAndAngle(m_orientation.rotatedVector({1, 0, 0}), // create rotation around the x
                                             diff.y() / (1 / m_rotateSpeed));        // axis rotate by current rotation
    rotate(rotation);
    buildViewMatrix();
}

void Camera::rotate(QQuaternion rotation) {
    m_orientation = rotation * m_orientation;
    buildViewMatrix();
}

void Camera::moveForward(int deltaT) {
    m_position += m_orientation.rotatedVector({0, 0, 1}) / (1 / (m_translateSpeed * deltaT / 30.0));
    buildViewMatrix();
}

void Camera::moveRight(int deltaT) {
    m_position += m_orientation.rotatedVector({1, 0, 0}) / (1 / (m_translateSpeed * deltaT / 30.0));
    buildViewMatrix();
}

void Camera::moveLeft(int deltaT) {
    m_position += m_orientation.rotatedVector({-1, 0, 0}) / (1 / (m_translateSpeed * deltaT / 30.0));
    buildViewMatrix();
}

void Camera::moveBackwards(int deltaT) {
    m_position += m_orientation.rotatedVector({0, 0, -1}) / (1 / (m_translateSpeed * deltaT / 30.0));
    buildViewMatrix();
}

void Camera::slowDown() {
    m_translateSpeed /= 1.5;
}

void Camera::speedUp() {
    m_translateSpeed *= 1.5;
}

void Camera::setOrientation(const QQuaternion &orientation) {
    this->m_orientation = orientation;
    buildViewMatrix();
}

//
// Created by kalterkrieg on 12/06/19.
//

#include "checkBox.h"

CheckBox::CheckBox(QString widgetName, QString variableName, bool state) :
    QCheckBox(widgetName)
{
    m_variableName = new char[sizeof(variableName)];
    strcpy(m_variableName, variableName.toStdString().c_str());
    setChecked(state);
}

CheckBox::~CheckBox() {
    delete[] m_variableName;
}

char *CheckBox::getVariableName() {
    return m_variableName;
}

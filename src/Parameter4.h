//
// Created by kalterkrieg on 13/06/19.
//

#ifndef IN55_PARAMETER4_H
#define IN55_PARAMETER4_H


#include <QtCore/QList>
#include "parameter.h"
#include <QVector4D>

class Parameter4 {
public:
    explicit Parameter4(const QString &name);
    Parameter4(const QString &name, double min, double max);
    Parameter4(const QString &name, double min, double max, double defaultValue);
    ~Parameter4();

    QHBoxLayout *getLayout();
    const char *getVariableName() const;

    const QVector4D toVec4() const;

private:
    QString m_name;
    QLabel *m_nameLabel;
    QList<Parameter *> m_parameterList;
    QHBoxLayout *m_layout;
    char *m_variableName;
};


#endif //IN55_PARAMETER4_H

#include "renderingwindow.h"

#include <QMouseEvent>

#include <cmath>
#include <iostream>
#include <QtWidgets/QStyle>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include "parameterwindow.h"


RenderingWindow::RenderingWindow(QWidget *parent, ParameterWindow *parameterWindow) :
        QOpenGLWidget(parent),
        m_camera({-1, 11, -15}, // default position and orientation if there is no save file
               {-0.990525484085083,
                -0.1374671459197998,
                -0.0004365349595900625,
                0.0004113912582397461}
        ),
        m_parameterWindow(parameterWindow) {

    resize(960, 960);

    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);

    loadCamera(); // load camera position and orientation if file exists

    m_vshader = new QFile("ressources/vshader.glsl");
    m_fshader = new QFile("ressources/fshader.glsl");

    m_program = new QOpenGLShaderProgram(context());

    m_fileSystemWatcher.addPath({"ressources/fshader.glsl"});

    connect(&m_fileSystemWatcher, SIGNAL(fileChanged(QString)),
            this, SLOT(initShaders())); // call initShaders whenever shader files have changed

    connect(parameterWindow->getFPSLimitCheckbox(), SIGNAL(stateChanged(int)),
            this, SLOT(setFPSLimit()));
}

RenderingWindow::~RenderingWindow() {
    makeCurrent();
    delete m_program;
    m_vshader->close();
    m_fshader->close();
    delete m_vshader;
    delete m_fshader;
    doneCurrent();
}


void RenderingWindow::keyPressEvent(QKeyEvent *e) {

    m_keys[e->key()] = true; // put corresponding entry to true in the array of keys

    if (m_keys[Qt::Key_Escape])
        emit quit();

    if (m_keys[Qt::Key_K])
        saveCamera();

    if (m_keys[Qt::Key_L])
        loadCamera();

    QWidget::keyPressEvent(e);
}


void RenderingWindow::keyReleaseEvent(QKeyEvent *e) {
    m_keys[e->key()] = false;
    QWidget::keyReleaseEvent(e);
}

void RenderingWindow::mousePressEvent(QMouseEvent *e) {
    m_keys[e->button()] = true;
    QWidget::mousePressEvent(e);
}

void RenderingWindow::mouseReleaseEvent(QMouseEvent *e) {
    m_keys[e->button()] = false;
    QWidget::mouseReleaseEvent(e);
}

void RenderingWindow::wheelEvent(QWheelEvent *e) {
    if (e->delta() == -120) //mousewheel down
        m_camera.slowDown();
    else if (e->delta() == 120) //mousewheel up
        m_camera.speedUp();

    QWidget::wheelEvent(e);
}

void RenderingWindow::timerEvent(QTimerEvent *e) {

    if (hasFocus()) {
        QPoint position = QCursor::pos();
        auto diff = QVector2D(position - m_prevMousePos);

        if(m_keys[Qt::LeftButton])
            m_camera.rotateFromMouseDiff(diff);

        if(m_keys[Qt::RightButton])
            m_camera.rotateFromMouseDiff(-diff);

        m_prevMousePos = position;

        if (m_keys[Qt::Key_Z])
            m_camera.moveForward(m_frameTime);

        if (m_keys[Qt::Key_Q])
            m_camera.moveLeft(m_frameTime);

        if (m_keys[Qt::Key_S])
            m_camera.moveBackwards(m_frameTime);

        if (m_keys[Qt::Key_D])
            m_camera.moveRight(m_frameTime);
    }

    update();

    ++m_frameCount;
    m_fps = m_frameCount / ((double) m_qtime.elapsed() / 1000.0);

    if (m_qtime.elapsed() >= 1000){ // refresh counter every second to have an up to date counter
        m_frameCount = 0;
        m_qtime.restart();
    }

    m_parameterWindow->getFPSLabel()->setText("fps : " + QLocale().toString(m_fps));
}


void RenderingWindow::initializeGL() {

    initializeOpenGLFunctions();

    glClearColor(0, 0, 0, 1);

    initShaders();

    // Use QBasicTimer because its faster than QTimer
    m_timer.start(m_frameTime, this);
    m_qtime = QTime();
    m_qtime.start();
}

void RenderingWindow::initShaders() {

    m_fileSystemWatcher.addPath({"ressources/fshader.glsl"});
    // needed because it seems clion doesnt modify file but delete and recreates it

    QOpenGLShaderProgram *previousProgram = m_program;           // keep reference to previous program in case new one does not compile
    auto *newProgram = new QOpenGLShaderProgram(context());

    m_vshader->open(QIODevice::ReadOnly);
    m_fshader->open(QIODevice::ReadOnly);

    QByteArray sv = m_vshader->readAll();
    QByteArray sf = m_fshader->readAll();

    sf = sf.right(sf.size()-12); // remove #version tag

    // add parameters to shader as uniform variables
    for(Parameter *param : this->m_parameterWindow->getParameterList()){
        auto a = QByteArray("uniform float ") + param->getVariableName() + QByteArray(";\n");
        sf = sf.prepend(a);
    }

    // add parameter4
    for(Parameter4 *param : this->m_parameterWindow->getParameter4List()){
        auto a = QByteArray("uniform vec4 ") + param->getVariableName() + QByteArray(";\n");
        sf = sf.prepend(a);
    }

    // add checkboxes
    for(CheckBox *checkBox : this->m_parameterWindow->getCheckBoxList()){
        auto a = QByteArray("uniform bool ") + checkBox->getVariableName() + QByteArray(";\n");
        sf = sf.prepend(a);
    }

    sf = sf.prepend("#version 330\n");

    if (newProgram->addShaderFromSourceCode(QOpenGLShader::Vertex, sv) && newProgram->addShaderFromSourceCode(QOpenGLShader::Fragment, sf))
    {
        // if new shader compiles, use it
        delete previousProgram;
        m_program = newProgram;
    }
    else // use previous shader
        delete newProgram;


    m_vshader->close();
    m_fshader->close();

    // Link shader pipeline
    if (!m_program->link())
        exit(1);

    // Bind shader pipeline for use
    if (!m_program->bind())
        exit(1);

    std::cout << "init shaders" << std::endl;

}

void RenderingWindow::resizeGL(int w, int h) {

    this->m_width = w;
    this->m_height = h;

}

bool RenderingWindow::saveCamera() {
    QFile saveFile("save.json");

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open save file.");
        return false;
    }

    QJsonObject camera;

    QJsonObject cameraJsonObject = camera["camera"].toObject();

    cameraJsonObject["PosX"] = m_camera.getPosition().x();
    cameraJsonObject["PosY"] = m_camera.getPosition().y();
    cameraJsonObject["PosZ"] = m_camera.getPosition().z();
    cameraJsonObject["OriS"] = m_camera.getOrientation().scalar();
    cameraJsonObject["OriX"] = m_camera.getOrientation().x();
    cameraJsonObject["OriY"] = m_camera.getOrientation().y();
    cameraJsonObject["OriZ"] = m_camera.getOrientation().z();

    QJsonDocument saveDoc(cameraJsonObject);
    saveFile.write(saveDoc.toJson());

    saveFile.close();

    return true;
}

bool RenderingWindow::loadCamera() {

    QFile loadFile("save.json");

    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open save file.");
        return false;
    }


    QByteArray saveData = loadFile.readAll();

    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));

    QJsonObject save = loadDoc.object();


    m_camera.setPosition({save["PosX"].toDouble(),
                        save["PosY"].toDouble(),
                        save["PosZ"].toDouble()});

    m_camera.setOrientation({save["OriS"].toDouble(),
                           save["OriX"].toDouble(),
                           save["OriY"].toDouble(),
                           save["OriZ"].toDouble()});

    loadFile.close();
    return true;

}


void RenderingWindow::setFPSLimit() {

    if(m_parameterWindow->getFPSLimitCheckbox()->isChecked()){
        m_frameTime = 50; // set limit to 20 fps

    } else {
        m_frameTime = 16; // set limit to 60 fps
    }

    m_timer.stop();
    m_timer.start(m_frameTime, this);
}


void RenderingWindow::paintGL() {

    m_program->setUniformValue("screenSize", this->m_width, this->m_height);
    m_program->setUniformValue("cam.position", this->m_camera.getPosition());
    m_program->setUniformValue("cam.viewMatrix", this->m_camera.getViewMatrix());
    m_program->setUniformValue("antialiasing", this->m_parameterWindow->getAntialiasingSpinbox()->value());

    for(Parameter *param : this->m_parameterWindow->getParameterList())
        m_program->setUniformValue(param->getVariableName(), (GLfloat )param->getValue());

    for(Parameter4 *param4 : this->m_parameterWindow->getParameter4List())
        m_program->setUniformValue(param4->getVariableName(), param4->toVec4());

    for(CheckBox *checkBox : this->m_parameterWindow->getCheckBoxList())
        m_program->setUniformValue(checkBox->getVariableName(), checkBox->isChecked());


    m_program->setUniformValue("object1Shape", (GLfloat )this->m_parameterWindow->getObject1Shape());
    m_program->setUniformValue("object2Shape", (GLfloat )this->m_parameterWindow->getObject2Shape());
    m_program->setUniformValue("object1Material", (GLfloat )this->m_parameterWindow->getObject1Material());
    m_program->setUniformValue("object2Material", (GLfloat )this->m_parameterWindow->getObject2Material());

    m_program->bind();

    if (m_program->isLinked())
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

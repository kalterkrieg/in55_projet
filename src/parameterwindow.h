//
// Created by kalterkrieg on 31/05/19.
//

#ifndef IN55_PARAMETERWINDOW_H
#define IN55_PARAMETERWINDOW_H

#include <QtWidgets/QSlider>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QPushButton>
#include <QtCore/QSignalMapper>

#include "parameter.h"
#include "checkBox.h"
#include "Parameter4.h"


#define GLASSMATERIAL 2
#define MIRRORMATERIAL 3
#define WALLMATERIAL 4

#define CUBESHAPE 1
#define SPHERESHAPE 2
#define DISTOSPHERESHAPE 3



class ParameterWindow : public QWidget
{
Q_OBJECT

public:
    explicit ParameterWindow(QWidget *parent = nullptr);
    ~ParameterWindow() override;

    QList<Parameter*> getParameterList();
    QList<CheckBox*> getCheckBoxList();
    QList<Parameter4*> getParameter4List();
    QCheckBox *getFPSLimitCheckbox() const;
    QLabel* getFPSLabel();
    QSpinBox *getAntialiasingSpinbox() const;
    int getObject1Shape() const;
    int getObject1Material() const;
    int getObject2Shape() const;
    int getObject2Material() const;

protected:
    void initPushButtons();

private:
    QList<Parameter*> m_paramList;      // list of float parameters
    QList<Parameter4*>  m_param4List;   // list of 4 dimensions parameter
    QList<CheckBox*> m_checkBoxList;    // list of checkboxes
    QList<QWidget*> m_otherWidgets;                  // needed to delete widgets in destructor
    QList<QHBoxLayout*> m_pushButtonLayoutList;      // needed to delete widgets in destructor
    QList<QSignalMapper*> m_signalMapperList;        // needed to delete widgets in destructor

    QCheckBox *m_FPSLimitCheckbox;
    QVBoxLayout *m_mainLayout;
    QLabel *m_fpsLabel;
    QSpinBox* m_antialiasingSpinbox;

    int m_object1Shape = CUBESHAPE;
    int m_object1Material = MIRRORMATERIAL;
    int m_object2Shape = CUBESHAPE;
    int m_object2Material = MIRRORMATERIAL;

protected slots:

    void setObject1Shape(int shape);
    void setObject2Shape(int shape);
    void setObject1Material(int material);
    void setObject2Material(int material);
};


#endif //IN55_PARAMETERWINDOW_H

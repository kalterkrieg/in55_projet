// adapted from https://stackoverflow.com/a/42427426

#include <QtWidgets/QDoubleSpinBox>
#include <QMouseEvent>
#include <limits>
#include <QtWidgets/QLabel>

class DoubleSpinBoxLabel : public QLabel {
public:
    explicit DoubleSpinBoxLabel(double startValue, double speed, QWidget *parent = nullptr);
    ~DoubleSpinBoxLabel() override;

    const double getValue() const;

    void setMinimum(double val);
    void setMaximum(double val);
    void setDefaultValue(double defaultValue);
    void setValue(double val);

protected:
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;

private:
    QDoubleSpinBox *m_spinBox;
    bool m_isDragging = false;
    QPoint m_previousPos;
    double m_value;
    double m_defaultValue;
    double m_speed;
};


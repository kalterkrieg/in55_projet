//
// Created by kalterkrieg on 12/06/19.
//

#ifndef IN55_CHECKBOX_H
#define IN55_CHECKBOX_H


#include <QtWidgets/QCheckBox>

class CheckBox : public QCheckBox{
public:
    CheckBox(QString widgetName, QString variableName, bool state = false);
    ~CheckBox() override;
    char *getVariableName();

private:
    char *m_variableName;
};


#endif //IN55_CHECKBOX_H

#include "spinboxlabel.h"

DoubleSpinBoxLabel::DoubleSpinBoxLabel(double startValue, double speed, QWidget *parent)
        : m_speed(speed),
          m_defaultValue(startValue) {

    m_spinBox = new QDoubleSpinBox(parent);
    m_spinBox->setValue(m_defaultValue);
    m_spinBox->setMinimum(std::numeric_limits<double>::lowest()); //needed as minimum is 0 by default
    m_spinBox->setMaximum(std::numeric_limits<double>::max());
    setStyleSheet("background-color:#525E68;border-radius:2px;");

    this->setText(QLocale().toString(m_defaultValue));
    m_value = m_spinBox->value();
}

void DoubleSpinBoxLabel::mouseMoveEvent(QMouseEvent *event) {

    if (!(event->buttons() & Qt::LeftButton))
        return QLabel::mouseMoveEvent(event);

    if (!m_isDragging) {
        m_previousPos = QCursor::pos();
        m_value = m_spinBox->value();
        m_isDragging = true;
    } else {
        int dragDist = QCursor::pos().x() - m_previousPos.x();
        m_previousPos = QCursor::pos();
        if (dragDist == 0)
            return;

        double dragMultiplier = m_speed / 1000 ;
        if (!(event->modifiers() & Qt::ControlModifier))
            dragMultiplier *= 10.0;

        m_value += dragMultiplier * dragDist;


        if (m_value > m_spinBox->maximum())
            m_value = m_spinBox->maximum();
        else if (m_value < m_spinBox->minimum())
            m_value = m_spinBox->minimum();

        m_spinBox->setValue(m_value);
        this->setText(QLocale().toString(m_value));


    }
}

void DoubleSpinBoxLabel::mouseReleaseEvent(QMouseEvent *event) {

    if (!m_isDragging || event->button() != Qt::LeftButton)
        return QLabel::mouseReleaseEvent(event);

    m_isDragging = false;
}

void DoubleSpinBoxLabel::mouseDoubleClickEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton) {
        setValue(m_defaultValue);

    } else
        QWidget::mouseDoubleClickEvent(event);
}

void DoubleSpinBoxLabel::setValue(double val) {
    m_spinBox->setValue(val);
    m_value = val;
    setText(QLocale().toString(m_spinBox->value()));
}

DoubleSpinBoxLabel::~DoubleSpinBoxLabel() {
    delete m_spinBox;
}

void DoubleSpinBoxLabel::setMinimum(double val) {
    m_spinBox->setMinimum(val);
}


void DoubleSpinBoxLabel::setMaximum(double val) {
    m_spinBox->setMaximum(val);
}

const double DoubleSpinBoxLabel::getValue() const {
    return m_value;
}

void DoubleSpinBoxLabel::setDefaultValue(double defaultValue) {
    m_defaultValue = defaultValue;
}


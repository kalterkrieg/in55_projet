//
// Created by kalterkrieg on 05/06/19.
//

#ifndef IN55_PARAMETER_H
#define IN55_PARAMETER_H

#include <QtCore/QString>
#include <QtWidgets/QHBoxLayout>
#include "spinboxlabel.h"

class Parameter {
public:
    explicit Parameter(double defaultValue = 0, double speed = 1);
    explicit Parameter(const QString &name, double defaultValue = 0, double speed = 1);
    Parameter(const QString &name, double defaultValue, double speed, double min, double max);
    ~Parameter();

    const double getValue() const ;
    QHBoxLayout *getLayout();
    const char* getVariableName() const;

    void setDefaultValue(double defaultValue);
    void setValue(double val);
    void setMin(double min);
    void setMax(double max);

private:
    QHBoxLayout *m_layout;
    DoubleSpinBoxLabel *m_spinbox;
    QLabel *m_label;
    char* m_variableName;
};

#endif //IN55_PARAMETER_H

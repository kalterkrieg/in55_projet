//
// Created by kalterkrieg on 31/05/19.
//

#include "parameterwindow.h"


ParameterWindow::ParameterWindow(QWidget *parent) :
        QWidget(parent) {
    setMinimumWidth(400);

    this->m_mainLayout = new QVBoxLayout();

    m_FPSLimitCheckbox = new QCheckBox("FPS limit"); //dont add in checkboxlist because not used in shader
    m_mainLayout->addWidget(m_FPSLimitCheckbox);


    m_checkBoxList.append(new CheckBox("Show number of steps", "showStepNumber"));
    m_checkBoxList.append(new CheckBox("Activate distance plane", "activateDistancePlane", true));
    m_checkBoxList.append(new CheckBox("Activate shadows", "activateShadows", true));


    for (CheckBox *checkbox : m_checkBoxList)
        m_mainLayout->addWidget(checkbox);


    m_paramList.append(new Parameter("planeHeight", 0, 4));
    m_paramList.append(new Parameter("refractionIndex", 1.2, 1, 1, 2));
    m_paramList.append(new Parameter("objectsSize", 1, 2, 0, 10));
    m_paramList.append(new Parameter("distortion", 5, 2, 0, 10));

    m_antialiasingSpinbox = new QSpinBox();
    m_antialiasingSpinbox->setValue(1);
    m_antialiasingSpinbox->setMinimum(1);
    m_antialiasingSpinbox->setMaximum(8);
    m_mainLayout->addWidget(m_antialiasingSpinbox);

    m_param4List.append(new Parameter4("materialTint", 0, 1, 1));

    m_fpsLabel = new QLabel();
    m_mainLayout->addWidget(m_fpsLabel);


    for (Parameter *param : m_paramList)
        m_mainLayout->addLayout(param->getLayout());

    for (Parameter4 *param : m_param4List)
        m_mainLayout->addLayout(param->getLayout());


    initPushButtons();

    setLayout(m_mainLayout);
}

ParameterWindow::~ParameterWindow() {

    for (Parameter *param : m_paramList)
        delete param;

    for (Parameter4 *param4 : m_param4List)
        delete param4;

    for (CheckBox *checkbox : m_checkBoxList)
        delete checkbox;

    for (QHBoxLayout *layout: m_pushButtonLayoutList)
        delete layout;

    for (QWidget *widget: m_otherWidgets)
        delete widget;

    for (QSignalMapper *signalMapper : m_signalMapperList)
        delete signalMapper;

    delete m_fpsLabel;
    delete m_mainLayout;
    delete m_antialiasingSpinbox;
    delete m_FPSLimitCheckbox;
}

QCheckBox *ParameterWindow::getFPSLimitCheckbox() const {
    return m_FPSLimitCheckbox;
}

QList<Parameter *> ParameterWindow::getParameterList() {
    return m_paramList;
}

QList<CheckBox *> ParameterWindow::getCheckBoxList() {
    return m_checkBoxList;
}

QList<Parameter4 *> ParameterWindow::getParameter4List() {
    return m_param4List;
}

QLabel *ParameterWindow::getFPSLabel() {
    return m_fpsLabel;
}

QSpinBox *ParameterWindow::getAntialiasingSpinbox() const {
    return m_antialiasingSpinbox;
}

int ParameterWindow::getObject1Shape() const {
    return m_object1Shape;
}

int ParameterWindow::getObject1Material() const {
    return m_object1Material;
}

int ParameterWindow::getObject2Shape() const {
    return m_object2Shape;
}

int ParameterWindow::getObject2Material() const {
    return m_object2Material;
}

void ParameterWindow::setObject1Shape(int shape) {
    m_object1Shape = shape;
}

void ParameterWindow::setObject2Shape(int shape) {
    m_object2Shape = shape;
}

void ParameterWindow::setObject1Material(int material) {
    m_object1Material = material;
}

void ParameterWindow::setObject2Material(int material) {
    m_object2Material = material;
}

void ParameterWindow::initPushButtons() {
    // creates all the push button widgets and labels to parametrize the objects

    auto * signalMapper = new QSignalMapper (this) ; // needed to pass parameter to slot

    m_signalMapperList.append(signalMapper);

    auto *layoutObj = new QHBoxLayout();
    m_pushButtonLayoutList.append(layoutObj);

    auto *label = new QLabel("Object 1 shape");
    m_otherWidgets.append(label);
    m_mainLayout->addWidget(label);

    auto *pushButton = new QPushButton("Cube");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));

    signalMapper -> setMapping (pushButton, CUBESHAPE) ;
    layoutObj->addWidget(pushButton);

    pushButton = new QPushButton("Sphere");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));
    signalMapper -> setMapping (pushButton, SPHERESHAPE) ;
    layoutObj->addWidget(pushButton);

    pushButton = new QPushButton("Distorted sphere");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));
    signalMapper -> setMapping (pushButton, DISTOSPHERESHAPE) ;
    layoutObj->addWidget(pushButton);

    connect (signalMapper, SIGNAL(mapped(int)), this, SLOT(setObject1Shape(int))) ;

    m_mainLayout->addLayout(layoutObj);

    signalMapper = new QSignalMapper(this);
    m_signalMapperList.append(signalMapper);

    layoutObj = new QHBoxLayout();
    m_pushButtonLayoutList.append(layoutObj);

    label = new QLabel("Object 1 material");
    m_otherWidgets.append(label);
    m_mainLayout->addWidget(label);


    pushButton = new QPushButton("Mirror");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));
    signalMapper -> setMapping (pushButton, MIRRORMATERIAL) ;
    layoutObj->addWidget(pushButton);

    pushButton = new QPushButton("Glass");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));
    signalMapper -> setMapping (pushButton, GLASSMATERIAL) ;
    layoutObj->addWidget(pushButton);

    m_mainLayout->addLayout(layoutObj);
    connect (signalMapper, SIGNAL(mapped(int)), this, SLOT(setObject1Material(int))) ;

    label = new QLabel("Object 2 shape");
    m_otherWidgets.append(label);
    m_mainLayout->addWidget(label);
    layoutObj = new QHBoxLayout();
    m_pushButtonLayoutList.append(layoutObj);

    signalMapper = new QSignalMapper (this) ;
    m_signalMapperList.append(signalMapper);

    pushButton = new QPushButton("Cube");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));
    signalMapper -> setMapping (pushButton, CUBESHAPE) ;
    layoutObj->addWidget(pushButton);

    pushButton = new QPushButton("Sphere");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));
    signalMapper -> setMapping (pushButton, SPHERESHAPE) ;
    layoutObj->addWidget(pushButton);

    pushButton = new QPushButton("Distorted sphere");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));
    signalMapper -> setMapping (pushButton, DISTOSPHERESHAPE) ;
    layoutObj->addWidget(pushButton);

    m_mainLayout->addLayout(layoutObj);

    connect (signalMapper, SIGNAL(mapped(int)), this, SLOT(setObject2Shape(int))) ;
    layoutObj = new QHBoxLayout();
    m_pushButtonLayoutList.append(layoutObj);

    signalMapper = new QSignalMapper (this) ;
    m_signalMapperList.append(signalMapper);

    label = new QLabel("Object 2 material");
    m_otherWidgets.append(label);
    m_mainLayout->addWidget(label);


    pushButton = new QPushButton("Mirror");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));
    signalMapper -> setMapping (pushButton, MIRRORMATERIAL) ;
    layoutObj->addWidget(pushButton);

    pushButton = new QPushButton("Glass");
    m_otherWidgets.append(pushButton);
    connect(pushButton, SIGNAL(pressed()),
            signalMapper, SLOT(map()));
    signalMapper -> setMapping (pushButton, GLASSMATERIAL) ;
    layoutObj->addWidget(pushButton);

    m_mainLayout->addLayout(layoutObj);

    connect (signalMapper, SIGNAL(mapped(int)), this, SLOT(setObject2Material(int))) ;

}

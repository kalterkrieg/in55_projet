//
// Created by kalterkrieg on 16/05/19.
//

#ifndef IN55_TP1_CAMERA_H
#define IN55_TP1_CAMERA_H


#include <QtGui/QVector3D>
#include <QtGui/QVector2D>
#include <QtGui/QQuaternion>
#include <QtGui/QMatrix4x4>
#include <QtCore/QElapsedTimer>

class Camera {

public:
    Camera(QVector3D position, QQuaternion orientation);
    Camera() = default;
    ~Camera() = default;

    void rotateFromMouseDiff(QVector2D diff);

    const QMatrix4x4 getViewMatrix();

    const QVector3D &getPosition() const;
    void setPosition(const QVector3D &position);

    const QQuaternion &getOrientation() const;
    void setOrientation(const QQuaternion &orientation);

    void moveForward(int deltaT);
    void moveRight(int deltaT);
    void moveLeft(int deltaT);
    void moveBackwards(int deltaT);

    void slowDown();
    void speedUp();


protected:
    void buildViewMatrix();

    void rotate(QQuaternion);


private:
    QVector3D m_position;
    QQuaternion m_orientation;
    QMatrix4x4 m_viewMatrix;
    float m_translateSpeed = 0.4;
    float m_rotateSpeed = 0.1;

};


#endif //IN55_TP1_CAMERA_H

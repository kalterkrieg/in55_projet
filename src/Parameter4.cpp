//
// Created by kalterkrieg on 13/06/19.
//

#include "Parameter4.h"

Parameter4::Parameter4(const QString &name)
        : m_name(name) {

    m_variableName = new char[sizeof(name)];
    strcpy(m_variableName, name.toStdString().c_str());

    for (int i = 0; i<4; ++i)
        m_parameterList.append(new Parameter());

    m_layout = new QHBoxLayout();

    m_nameLabel = new QLabel(name);


    for (Parameter *param : m_parameterList)
        m_layout->addLayout(param->getLayout());

    m_layout->addWidget(m_nameLabel);

}


Parameter4::Parameter4(const QString &name, double min, double max)
        : Parameter4(name) {

    for (Parameter *param : m_parameterList) {
        param->setValue(min);
        param->setMin(min);
        param->setMax(max);
    }
}



Parameter4::Parameter4(const QString &name, double min, double max, double defaultValue)
        : Parameter4(name, min, max) {

    for (Parameter *param : m_parameterList) {
        param->setDefaultValue(defaultValue);
        param->setValue(defaultValue);
    }
}



Parameter4::~Parameter4() {

    for (Parameter *param : m_parameterList)
        delete param;

    delete m_layout;
    delete[] m_variableName;


}

QHBoxLayout *Parameter4::getLayout() {
    return m_layout;
}

const char *Parameter4::getVariableName() const {
    return m_variableName;
}

const QVector4D Parameter4::toVec4() const {
    return {m_parameterList[0]->getValue(),
            m_parameterList[1]->getValue(),
            m_parameterList[2]->getValue(),
            m_parameterList[3]->getValue()};
}

//
// Created by kalterkrieg on 05/06/19.
//

#include "parameter.h"


Parameter::Parameter(double defaultValue, double speed) {

    m_spinbox = new DoubleSpinBoxLabel(defaultValue, speed);
    m_layout = new QHBoxLayout();
    m_layout->addWidget(m_spinbox);
    m_label = nullptr;
    m_variableName = nullptr;
}


Parameter::Parameter(const QString &name, double defaultValue, double speed) {

    m_spinbox = new DoubleSpinBoxLabel(defaultValue, speed);
    m_label = new QLabel(name);
    m_variableName = new char[sizeof(name)];
    strcpy(m_variableName, name.toStdString().c_str());
    m_layout = new QHBoxLayout();
    m_layout->addWidget(m_spinbox);
    m_layout->addWidget(m_label);

}

Parameter::Parameter(const QString &name,
                     double defaultValue,
                     double speed,
                     double min,
                     double max) :
        Parameter(name, defaultValue, speed) {
    m_spinbox->setMinimum(min);
    m_spinbox->setMaximum(max);
}

Parameter::~Parameter() {

    delete m_label;
    delete[] m_variableName;
    delete m_spinbox;
    delete m_layout;
}

QHBoxLayout *Parameter::getLayout() {
    return m_layout;
}

const char *Parameter::getVariableName() const {
    return m_variableName;
}

const double Parameter::getValue() const {
    return m_spinbox->getValue();
}

void Parameter::setMin(double min) {
    m_spinbox->setMinimum(min);

}

void Parameter::setMax(double max) {
    m_spinbox->setMaximum(max);

}

void Parameter::setDefaultValue(double defaultValue) {
    m_spinbox->setDefaultValue(defaultValue);
}

void Parameter::setValue(double val) {
    m_spinbox->setValue(val);

}
